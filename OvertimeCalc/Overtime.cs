﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OvertimeCalc
{
    public class Overtime
    {
        public event EventHandler NotifyEvent;


        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _startTime;

        public DateTime StartTime
        {
            get { return _startTime; }
            set
            {
                _startTime = value;
                _sumTime = _endTime.Subtract(_startTime).TotalMinutes;
                OnNotify();
            }
        }
        private DateTime _endTime;

        public DateTime EndTime
        {
            get { return _endTime; }
            set
            {
                _endTime = value;
                _sumTime = _endTime.Subtract(_startTime).TotalMinutes;
                OnNotify();
            }
        }

        private double _sumTime;

        public double SumTime
        {
            get { return _sumTime; }
            set { _sumTime = value; }
        }

        private void OnNotify()
        {
            if (NotifyEvent != null)
            {
                NotifyEvent(this, null);
            }
        }
    }
}
